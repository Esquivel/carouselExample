package com.carouselapp.ui;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carouselapp.Constants;
import com.carouselapp.R;
import com.carouselapp.domain.Widget;

import java.util.List;

public class WidgetListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
	
	private List<Widget> widgetList;
	private Context context;
	
	public WidgetListAdapter(List<Widget> widgetList, Context context) {
		this.context = context;
		this.widgetList = widgetList;
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		RecyclerView.ViewHolder viewHolder;
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.recycler_list, parent, false);
		viewHolder = new WidgetItemHolder(view);
		return viewHolder;
	}
	
	@Override
	public int getItemViewType(int position) {
		switch (widgetList.get(position).getType()) {
			case Constants.THUMB:
				return 1;
			case Constants.POSTER:
				return 2;
			default:
				return 3;
		}
	}
	
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		RecyclerView.Adapter adapter;
		switch (holder.getItemViewType()) {
			case 1:
				adapter = new ThumbAdapter(widgetList.get(position).getItems(), context);
				break;
			case 2:
				adapter = new PosterAdapter(widgetList.get(position).getItems(), context);
				break;
			default:
				adapter = new PosterAdapter(widgetList.get(position).getItems(), context);
		}
		
		((WidgetItemHolder)holder).title.setText(widgetList.get(position).getTitle());
		((WidgetItemHolder)holder).recycler.setHasFixedSize(true);
		((WidgetItemHolder)holder).recycler.setLayoutManager(
				new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
		((WidgetItemHolder)holder).recycler.setAdapter(adapter);
		((WidgetItemHolder)holder).recycler.setNestedScrollingEnabled(false);
	}
	
	public void addList(List<Widget> widgets) {
		widgetList = widgets;
		notifyDataSetChanged();
	}
	
	@Override
	public int getItemCount() {
		return widgetList.size();
	}
	
	private class WidgetItemHolder extends RecyclerView.ViewHolder {
		
		private RecyclerView recycler;
		private TextView title;
		
		private WidgetItemHolder(View view) {
			super(view);
			title = (TextView)view.findViewById(R.id.title);
			recycler = (RecyclerView)view.findViewById(R.id.recycler);
		}
	}
}
