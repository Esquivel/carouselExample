package com.carouselapp.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carouselapp.R;
import com.carouselapp.domain.Items;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PosterAdapter extends RecyclerView.Adapter<PosterAdapter.PosterItemHolder> {
	
	private List<Items> itemsList;
	private Context context;
	
	public PosterAdapter(List<Items> itemsList, Context context) {
		this.itemsList = itemsList;
		this.context = context;
	}
	
	@Override
	public PosterItemHolder onCreateViewHolder(ViewGroup parent, int i) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poster_item, null);
		return new PosterItemHolder(view);
	}
	
	@Override
	public void onBindViewHolder(PosterItemHolder holder, int i) {
		holder.textPoster.setText(itemsList.get(i).getTitle());
		Picasso.with(context).load(itemsList.get(i).getUrl())
				.placeholder(R.drawable.placeholder)
				.fit().centerCrop().into(holder.imagePoster);
	}
	
	@Override
	public int getItemCount() {
		return itemsList.size();
	}
	
	class PosterItemHolder extends RecyclerView.ViewHolder {
		
		TextView textPoster;
		ImageView imagePoster;
		
		PosterItemHolder(View view) {
			super(view);
			textPoster = (TextView)view.findViewById(R.id.textPoster);
			imagePoster = (ImageView)view.findViewById(R.id.imagePoster);
		}
	}
}