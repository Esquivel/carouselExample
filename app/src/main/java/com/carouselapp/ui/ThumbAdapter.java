package com.carouselapp.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carouselapp.R;
import com.carouselapp.domain.Items;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ThumbAdapter extends RecyclerView.Adapter<ThumbAdapter.ThumbItemHolder> {
	
	private List<Items> itemsList;
	private Context context;
	
	public ThumbAdapter(List<Items> itemsList, Context context) {
		this.itemsList = itemsList;
		this.context = context;
	}
	
	@Override
	public ThumbItemHolder onCreateViewHolder(ViewGroup parent, int i) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.thumb_item, null);
		return new ThumbItemHolder(view);
	}
	
	@Override
	public void onBindViewHolder(ThumbItemHolder holder, int i) {
		holder.textThumb.setText(itemsList.get(i).getTitle());
		Picasso.with(context).load(itemsList.get(i).getUrl())
				.placeholder(R.drawable.placeholder)
				.fit().centerCrop().into(holder.imageThumb);
	}
	
	@Override
	public int getItemCount() {
		return itemsList.size();
	}
	
	class ThumbItemHolder extends RecyclerView.ViewHolder {
		
		TextView textThumb;
		ImageView imageThumb;
		
		ThumbItemHolder(View view) {
			super(view);
			textThumb = (TextView)view.findViewById(R.id.textThumb);
			imageThumb = (ImageView)view.findViewById(R.id.imageThumb);
		}
	}
}