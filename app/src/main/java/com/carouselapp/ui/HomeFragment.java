package com.carouselapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.carouselapp.Constants;
import com.carouselapp.R;
import com.carouselapp.domain.Widget;
import com.carouselapp.service.WidgetService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, WidgetService.IWidgetService {
	
	private WidgetListAdapter widgetListAdapter;
	private SwipeRefreshLayout swipeRefresh;
	private List<Widget> widgetList = new ArrayList<>();
	private WidgetService widgetService;
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Constants.WIDGET_LIST, (Serializable)widgetList);
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		widgetService = WidgetService.getInstance();
		
		if (savedInstanceState != null) {
			widgetList = (List<Widget>)savedInstanceState.getSerializable(Constants.WIDGET_LIST);
		}
	}
	
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.home_fragment, container, false);
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		RecyclerView recyclerHome = (RecyclerView)view.findViewById(R.id.recyclerHome);
		
		widgetListAdapter = new WidgetListAdapter(widgetList, getContext());
		recyclerHome.setAdapter(widgetListAdapter);
		recyclerHome.setLayoutManager(new LinearLayoutManager(getActivity()));
		
		swipeRefresh = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefresh);
		swipeRefresh.setOnRefreshListener(this);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (widgetList.size() == 0) {
			swipeRefresh.setRefreshing(true);
			widgetService.getWidgetList(this);
		}
	}
	
	@Override
	public void onDestroy() {
		widgetService.unsubscribe();
		super.onDestroy();
	}
	
	private void populateAdapter(List<Widget> widgets) {
		widgetList = widgets;
		widgetListAdapter.addList(widgetList);
	}
	
	@Override
	public void onRefresh() {
		swipeRefresh.setRefreshing(true);
		widgetService.getWidgetList(this);
	}
	
	@Override
	public void getWidgetServiceOk(List<Widget> widgets) {
		swipeRefresh.setRefreshing(false);
		populateAdapter(widgets);
	}
	
	@Override
	public void getWidgetServiceError(Throwable e) {
		swipeRefresh.setRefreshing(false);
		Toast.makeText(getContext(), R.string.error_message, Toast.LENGTH_SHORT).show();
	}
}
