package com.carouselapp.domain;

import java.io.Serializable;

public class Items implements Serializable {
	
	private String title;
	private String url;
	
	public Items(String title, String url) {
		this.title = title;
		this.url = url;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
}
