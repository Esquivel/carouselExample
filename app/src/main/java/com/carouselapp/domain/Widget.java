package com.carouselapp.domain;

import java.io.Serializable;
import java.util.List;

public class Widget implements Serializable {
	
	private String title;
	private String type;
	private List<Items> items;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public List<Items> getItems() {
		return items;
	}
	
	public void setItems(List<Items> items) {
		this.items = items;
	}
}
