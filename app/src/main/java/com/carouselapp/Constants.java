package com.carouselapp;

public class Constants {
	
	public static final String THUMB = "thumb";
	public static final String POSTER = "poster";
	public static final String WIDGET_LIST = "widget_list";
}
