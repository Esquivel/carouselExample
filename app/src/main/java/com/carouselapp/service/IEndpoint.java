package com.carouselapp.service;

import com.carouselapp.domain.Widget;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface IEndpoint {
	
	@GET("v2/{url}")
	Observable<List<Widget>> getData(@Path("url") String url);
}
