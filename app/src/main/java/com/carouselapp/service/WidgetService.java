package com.carouselapp.service;

import android.util.Log;

import com.carouselapp.ConfigUrlMocked;
import com.carouselapp.domain.Widget;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WidgetService {

	public interface IWidgetService {
		void getWidgetServiceOk(List<Widget> widgets);
		void getWidgetServiceError(Throwable e);
	}
	
	private static final String LOG_TAG = WidgetService.class.getName();
	private Subscription subscription;
	private IEndpoint iEndpoint;
	private IWidgetService iWidgetService;
	private static WidgetService instance;
	
	public static WidgetService getInstance() {
		if (instance == null) {
			instance = new WidgetService();
		}
		return instance;
	}
	
	private WidgetService() {
		init();
	}
	
	private void init() {
		getInstanceEndpoint();
	}
	
	private IEndpoint getInstanceEndpoint() {
		if (iEndpoint == null) {
			RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();
			Retrofit retrofit = new Retrofit.Builder().baseUrl(ConfigUrlMocked.BASE_URL)
					.addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(rxAdapter).build();
			iEndpoint = retrofit.create(IEndpoint.class);
		}
		return iEndpoint;
	}
	
	public void getWidgetList(IWidgetService iWidgetService) {
		this.iWidgetService = iWidgetService;
		Observable<List<Widget>> call = iEndpoint.getData(ConfigUrlMocked.MOCKEY_URL_GENERATED);
		subscription = call.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(subscriberForWidgetList());
	}
	
	private Subscriber<List<Widget>> subscriberForWidgetList() {
		return new Subscriber<List<Widget>>() {
			@Override
			public void onCompleted() {
				Log.v(LOG_TAG, "onCompleted");
			}
			
			@Override
			public void onError(Throwable e) {
				Log.v(LOG_TAG, "onError");
				iWidgetService.getWidgetServiceError(e);
			}
			
			@Override
			public void onNext(List<Widget> widgets) {
				Log.v(LOG_TAG, "onNext");
				iWidgetService.getWidgetServiceOk(widgets);
			}
		};
	}
	
	public void unsubscribe() {
		if (subscription != null && subscription.isUnsubscribed()) {
			subscription.unsubscribe();
		}
	}
}
